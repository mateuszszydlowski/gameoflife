﻿namespace GameOfLife
{
    public class Point
    {
        private int x;
        private int y;
        private bool isLive = false;
        private int numberOfNeighbours;
        public Point(int h,int w)
        {
            x = h;
            y = w;
        }
        public Point(Point point)
        {
            x = point.getX();
            y = point.getY();
            numberOfNeighbours = point.getNumberOfNeighbours();
            isLive = point.getIsLive();
        }

        public bool getIsLive()
        {
            return isLive;
        }
        public void setIsLive(bool l)
        {
            isLive = l;
        }
        public void setNumberOfNeighbours(int n)
        {
            numberOfNeighbours = n;
        }

        public int getNumberOfNeighbours()
        {
            return numberOfNeighbours;
        }

        public int getX()
        {
            return x;
        }
        public int getY()
        {
            return y;
        }
        public override string ToString()
        {
            if (isLive == true)
                return "*";
            else
                return "-";
        }
    }
}