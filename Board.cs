﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameOfLife
{
    public class Board
    {
        private List<List<Point>> list;

        public List<List<Point>> getList()
        {
            return list;
        }
        public void setList(List<List<Point>> newList)
        {
            list = newList;
        }
        public Board(int height, int width)
        {
            list = new List<List<Point>>();
            Random rand = new Random();
            for (int i = 0; i < height; i++)
            {
                list.Add(new List<Point>());
                for (int j = 0; j < width; j++)
                {
                    list[i].Add(new Point(i, j));
                    if (rand.Next(0, 2) == 0)
                        list[i][j].setIsLive(true);
                    
                }
            }

        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < list[i].Count; j++)
                {
                    sb.Append(list[i][j]);
                }

                sb.Append("\n");
            }
            return sb.ToString();
        }
        private int countNeighbours(Point point)
        {
            int countNeighbours = 0;
            int i = point.getX();
            int j = point.getY();
            try{
                if (list[i - 1][j - 1].getIsLive() == true)
                    countNeighbours++;
            } catch (ArgumentOutOfRangeException e) {}
            try{
                if (list[i - 1][j].getIsLive() == true)
                    countNeighbours++;
            }catch (ArgumentOutOfRangeException e) { }
            try{
                if (list[i - 1][j + 1].getIsLive() == true)
                    countNeighbours++;
            }catch (ArgumentOutOfRangeException e) { }
            try{
                if (list[i][j - 1].getIsLive() == true)
                    countNeighbours++;
            }catch (ArgumentOutOfRangeException e) { }
            try{
                if (list[i][j + 1].getIsLive() == true)
                    countNeighbours++;
            }catch (ArgumentOutOfRangeException e) { }
            try{
                if (list[i + 1][j].getIsLive() == true)
                    countNeighbours++;
            }catch (ArgumentOutOfRangeException e) { }
            try{
                if (list[i + 1][j + 1].getIsLive() == true)
                    countNeighbours++;
            }catch (ArgumentOutOfRangeException e) { }
            try{
                if (list[i + 1][j - 1].getIsLive() == true)
                    countNeighbours++;
            }catch (ArgumentOutOfRangeException e) { }
            return countNeighbours;
        }
        public void setsAllNeighbours()
        {
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < list[i].Count; j++)
                {
                    list[i][j].setNumberOfNeighbours(countNeighbours(list[i][j]));
                }

            }
        }

    }
}