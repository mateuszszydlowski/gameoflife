﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        
        static void Main(string[] args)
        {
            play();
        }

        public static void play()
        {
            Game game = new Game();
            Console.Write(game.getBoard());
            while (true)
            {
                String a = Console.ReadLine();
                game.getBoardAfterOneClick();
                Console.Write(game.getBoard());
            }
        }
    }
}
