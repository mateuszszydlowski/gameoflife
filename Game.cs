﻿using System;
using System.Collections.Generic;

namespace GameOfLife
{
    public class Game
    {
        private Board board = new Board(3,3);

        public Board getBoard()
        {
            return board;
        }

        
        public void getBoardAfterOneClick()
        {
            List<List<Point>> listToReturn = new List<List<Point>>();
            board.setsAllNeighbours();
            List<List<Point>> playList = board.getList();
            for (int i = 0; i < playList.Count; i++)
            {
                listToReturn.Add(new List<Point>());
                for (int j = 0; j < playList[i].Count; j++)
                {
                    Point point = new Point(playList[i][j]);
                    point.setIsLive(false); 
                    if (shouldBeLive(playList[i][j]) == true)
                    {
                        point.setIsLive(true);
                    }
                    listToReturn[i].Add(point);
                }
            }
            board.setList(listToReturn);
        }
        private bool shouldBeLive(Point point)
        {
            int w = point.getNumberOfNeighbours();
            if (w < 2) return false;
            else if (w == 2 && point.getIsLive()) return true;
            else if (w == 3) return true;
            return false;
        }
        
    }
}